(() => {
  class Logger {
    constructor() {
      this.loggerInfo = {
        default: {
          bgColor: 'transparent',
          textColor: 'white',
        },
        debug: {
          bgColor: 'teal',
          textColor: 'white',
        },
        error: {
          bgColor: 'crimson',
          textColor: 'white',
        },
        info: {
          bgColor: 'cornflowerblue',
          textColor: 'black',
        },
        trace: {
          bgColor: 'tan',
          textColor: 'black',
        },
        warn: {
          bgColor: 'darkorange',
          textColor: 'black',
        },
        namespace: {
          bgColor: 'indigo',
          textColor: 'white',
        },
        init: {
          bgColor: 'forestgreen',
          textColor: 'white',
        },
        initError: {
          bgColor: 'darkred',
          textColor: 'black',
        },
      };
      this.loggerLevels = Object.keys(this.loggerInfo);
    }

    get namespace() {
      return this.constructor.name;
    }

    getTagStyle = (level) => {
      const { textColor, bgColor } = this.loggerInfo[level];
      return `background-color:${bgColor};font-size:12px;font-weight:bold;color:${textColor};padding:1px 5px;`;
    };
    timeTag = () => {
      const time = new Date();
      const style = [
        'font-size:12px;color:gray;padding-left:4px;',
        'font-size:12px;color:#af2eba;',
        'font-size:12px;color:gray;padding-right:4px;',
      ];
      const text = `%c[%c${time.toLocaleDateString('pl').replaceAll('.', '/')} ${time.toLocaleTimeString('pl')}%c]`;

      return { text, style };
    };
    log = (namespace, loggerLevel, message) => {
      const time = this.timeTag();
      if (!this.loggerLevels.includes(loggerLevel.toLowerCase())) {
        return console.log(
          `${time.text}%cERROR%cTaki stopień logowania nie istnieje!`,
          ...time.style,
          this.getTagStyle('error'),
          this.getTagStyle('default'),
        );
      }

      console.log(
        `${time.text}%c${namespace}%c${loggerLevel.toUpperCase()}`,
        ...time.style,
        this.getTagStyle('namespace'),
        this.getTagStyle(loggerLevel),
        message,
      );
    };
    test = () => this.loggerLevels.forEach((level) => this.log(this.namespace, level, 'test'));
  }

  window.Logger = new Logger();
})();
