(() => {
  class WIH {
    constructor() {
      this.root = $('#frame_container');
      this.namespace = this.constructor.name;
      this.observer = {
        observedElement: $('#transition_container'),
        instance: null,
        settings: {
          attributes: true,
          attributeFilter: ['style'],
        },
      };

      this.availableTabs = ['monsters', 'players', 'npcs'];
      this._currentTab = this.availableTabs[0];

      this.modal = null;
      this.modalTrigger = null;
      this.isModalOpen = false;

      $('head').append(`
        <style>
          .${this.namespace}-hidden {
            opacity: 0;
            visibility: hidden;
          }

          .${this.namespace}-modal-trigger {
            background-color: hsl(207deg 18% 78%);
            position: fixed;
            left: 10px;
            top: 160px;
            border-radius: 50%;
            padding: 8px;
            box-sizing: content-box;
            cursor: url(https://www.norroth.com/styles/hand.cur) 4 0, pointer;
            filter: invert(1);
          }

          .${this.namespace}-button-container {
            display: flex;
            gap: 16px;
          }

          .${this.namespace}-content-container {
            width: 350px;
            max-height: 600px;
            display: flex;
            flex-direction: column;
            overflow: hidden;
            margin-top: -30px;
            gap: 8px;
            margin-bottom: 0;
          }

          .${this.namespace}-map-title {
            text-align: center;
            padding: 0;
            margin: 0;
            flex-shrink: 0;
          }

          #${this.namespace}-content {
            overflow: auto;
            padding-top: 0;
          }

          .${this.namespace}-button {
            background-color: hsl(27deg 18% 22%);
            border-radius: 6px;
            border: 1.5px solid hsl(27deg 18% 10%);
            cursor: url(https://www.norroth.com/styles/hand.cur) 4 0, pointer;
            transition: background-color 0.3s ease;
            text-transform: capitalize;
          }

          .${this.namespace}-button-active {
            background-color: hsl(27deg 18% 15%);
          }

          .${this.namespace}-list {
            display: flex;
            flex-direction: column;
            gap: 8px;
          }

          .${this.namespace}-item {
            display: flex;
            border-bottom: 1px dashed hsl(27deg 18% 15%);
            justify-content: space-between;
            gap: 8px;
            padding-bottom: 4px;
          }

          .${this.namespace}-item span {
            display: flex;
            align-items: center;
            cursor: url(https://www.norroth.com/styles/hand.cur) 4 0, pointer;
            padding: 0 4px 0 10px;
            white-space: nowrap;
          }

          .${this.namespace}-left {
            display: flex;
            flex: 1;
            gap: 8px;
            min-width: 0;
            align-items: center;
          }

          .${this.namespace}-left p {
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
          }
        </style>
      `);
    }

    init = () => {
      Logger.log(this.namespace, 'init', `Initializing...`);
      try {
        this.initObserver();
        const modalPos = this.modalPos;
        const name = this.namespace;

        this.modalTrigger = $(`
          <img
            src='https://www.svgrepo.com/show/127575/location-sign.svg'
            height='24'
            width='24'
            class='${name}-modal-trigger'
          />
        `).on('click', this.toggleModal);

        this.modal = $(`
          <div class='frame-container ${name}-hidden' id='${name}-frame' style="left: ${modalPos.x}px; top: ${
          modalPos.y
        }px">
            <div class='frame-title ui-draggable-handle'>
              <img src='/images/layout/frame-title-left.png'>
                Who is here
              <img src='/images/layout/frame-title-right.png'>
            </div>
            <div class='frame-close tooltip2' content='Close' title></div>
            <div class='frame-content ${name}-content-container'>
              <h3 class="col-xs-12 ${name}-map-title">${map.name}   |   lvl range: ${map.level_range}</h3>
              <div class="${name}-button-container">
                ${this.availableTabs
                  .map(
                    (tab) => `
                  <button
                    class='col-xs-${Math.floor(12 / this.availableTabs.length)} ${name}-button'
                    onClick="window['${name}'].currentTab = '${tab}'"
                  >${tab}</button>
                `,
                  )
                  ?.join('')}
              </div>
              <ul class='col-xs-12' id='${name}-content'></ul>
            </div>
          </div>
        `);
        this.modal.find('div[content=Close]').on('click', this.toggleModal);

        this.updateModal();

        $('.character_hud').append(this.modalTrigger);
        this.root.append(this.modal);
        $(this.modal).draggable({
          stop: (_, { position }) => {
            const { left, top } = position;

            this.modalPos = { x: left, y: top };
          },
        });
      } catch (e) {
        Logger.log(this.namespace, 'initError', 'Error during initialization...');
        console.error(e);
        return;
      }
      Logger.log(this.namespace, 'init', `Initialized.`);
    };
    initObserver = () => {
      let isUpdatingLocked = false;
      let imagesLoaded = 0;

      const updateModalAfterImages = (count) => {
        imagesLoaded++;
        if (imagesLoaded === count) this.updateModal();
      };

      const observerCallback = (mutations) => {
        const top = parseFloat(mutations[0].target.style.top);
        const height = parseFloat(mutations[0].target.style.height);

        if (top > 0 && top < 100 && height === 100 && !isUpdatingLocked && $('#map_container')[0]) {
          isUpdatingLocked = true;
          const imagesAmount = $('img').length;
          $('img').each((_, img) => {
            $('<img>')
              .on('load', () => updateModalAfterImages(imagesAmount))
              .attr('src', $(img).attr('src'));
          });
          this.updateModal();
        }

        if (isUpdatingLocked && top === 100 && top === 100) {
          isUpdatingLocked = false;
        }
      };

      this.observer.instance = new MutationObserver(observerCallback);
      this.observer.instance.observe(this.observer.observedElement[0], this.observer.settings);
    };
    toggleModal = () => {
      this.isModalOpen = !this.isModalOpen;
      this.modal.toggleClass(`${this.namespace}-hidden`, !this.isModalOpen);
    };
    updateModal = () => {
      const data = this[this.currentTab] || [];
      const newList = this.renderList(data);

      this.modal.find(`#${this.namespace}-content`).html(newList);
      this.modal.find(`.${this.namespace}-map-title`).html(`${map.name}   |   lvl range: ${map.level_range}`);
      $(this.modal)
        .find('button')
        .toArray()
        .forEach((button) => button.classList.remove(`${this.namespace}-button-active`));
      this.modal.find(`button:contains('${this.currentTab}')`).addClass(`${this.namespace}-button-active`);
    };
    renderList = (arr) => {
      const nameRegex = /\D+ /g;
      return arr.length > 0
        ? `
        <ul class="${this.namespace}-list">
          ${arr
            .map((entity) => {
              const { name, entityImage, level, rarity, entityX, entityY } = entity;

              return `
              <li class="${this.namespace}-item">
                <div class="${this.namespace}-left">
                  <img
                    src='${entityImage}'
                    height='48'
                  />
                  <p>${name}${level ? ` (${level})` : ''}</p>
                  ${rarity ? `<img src='${rarity}' />` : ''}
                </div>
                <span onClick="walkTo(${entityX}, ${entityY})">${entityX}, ${entityY}</span>
              </li>
            `;
            })
            .join('')}
        </ul>
      `
        : '<ul><li>Nothing to display.</li></ul>';
    };
    mapToObjectsArray = (sortableObj) => {
      const { isEnemy, list } = sortableObj;

      return list.map((entity) => {
        const ref = $(entity);

        const entityX = Math.floor((entity.offsetLeft + (entity.offsetWidth - 48) / 2) / 48);
        const entityY = (entity.offsetTop + entity.offsetHeight - 48) / 48;
        const entityImage = ref.find('img').prop('src');
        const rarity = ref.find('span.char_name_tag img').prop('src');

        const nameTag = ref.find('b.char_name_tag')[0];
        const entityNameNodeIndex = Number(
          !!(nameTag.childNodes.length > 1 ? nameTag.childNodes[0].textContent : null),
        );
        const entityNameText = nameTag.childNodes[entityNameNodeIndex].textContent;

        const [_, name, level] = entityNameText.match(/(.+) \((\d+)\)/);
        const action = null;

        return { entityImage, name, level: parseInt(level), rarity, entityX, entityY, action };
      });
    };
    npcMapper = (list) => {
      return list.map((node) => {
        node = $(node);
        const name = node.attr('name');
        const action = node.attr('action');
        const position = node
          .attr('coords')
          .split('-')
          .map((pos) => parseInt(pos));
        const entityImage = node.css('background-image').match(/"(.+)"/);
        const level = 0;

        return {
          name,
          level,
          entityImage: entityImage ? entityImage[1] : null,
          entityX: position[0],
          entityY: position[1],
          action,
        };
      });
    };
    sorter = (el1, el2) => {
      if (el1.action === 'gate' || el2.action === 'gate') return 1;

      return el1.name.localeCompare(el2.name) || el1.level - el2.level;
    };
    transform = (sortableObj) => this.mapToObjectsArray(sortableObj).sort(this.sorter);

    get currentTab() {
      return this._currentTab;
    }

    get modalPos() {
      const currentState = getJSON();
      const { x, y } = currentState[this.namespace].modalPos;

      return { x, y };
    }

    set modalPos(coords) {
      const { x, y } = coords;
      const currentState = getJSON();

      currentState[this.namespace].modalPos = { x, y };

      setJSON(currentState);
    }

    set currentTab(tab) {
      if (!this.availableTabs.includes(tab)) {
        Logger.log(this.namespace, 'error', `${tab} does not exists on available tabs to display!`);
        return;
      }

      this._currentTab = tab;
      this.updateModal();
    }

    get monsters() {
      return this.transform({ list: $('span.enemy').toArray(), isEnemy: true });
    }

    get players() {
      return this.transform({ list: $('span.player').toArray(), isEnemy: false });
    }

    get npcs() {
      const arr = $('.map_table div[action]:not([action=combat])').toArray();
      const mappedData = this.npcMapper(arr);
      return mappedData.sort(this.sorter);
    }
  }

  window.WIH = new WIH();
  window.WIH.init();
})();
