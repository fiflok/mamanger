(() => {
  class WTG {
    constructor() {
      this.currentCanvas = null;
      this.namespace = this.constructor.name;
      this.walkable = {
        list: [],
        lastTile: {
          x: null,
          y: null,
        },
      };
      this.initSetup = {
        counter: 0,
        limit: 5,
        interval: 150,
        wasInitialized: false,
      };
      this.tileSize = 48;
      this.observer = {
        instance: null,
        settings: {
          childList: true,
        },
        observedElement: $('#window_map'),
      };
      this.currentlyWalkingTo = null;
      this.colors = {
        tile: '',
        tileBorder: '',
        tileAmbush: '',
        tilePath: '',
      };
    }

    init = () => {
      if (++this.initSetup.counter === this.initSetup.limit) {
        Logger.log(this.namespace, 'initError', 'Reached limit of intialization retries... Aborting!');
        notification(`${this.namespace}: Osiągnięto limit prób konfiguracji, powiadom developera.`, 'red', true);
        return;
      }

      Logger.log(this.namespace, 'init', `Initializing...`);
      try {
        if (!window.nextStep || !window.walkTo) {
          Logger.log(this.namespace, 'initError', 'nextStep or walkTo functions not found, retrying...');
          setTimeout(window[this.namespace].init, this.initSetup.interval);
          return;
        }

        window.nextStep = (function () {
          const cached = window.nextStep;

          return function () {
            const characterId = arguments[0];
            if (characterId === character.id) {
              const remapQueue = walkQueue[characterId].map((tile) => {
                const coordsString = tile.split('-');
                const coords = coordsString.map((num) => parseInt(num));
                return { x: coords[0], y: coords[1], isAmbush: coordsString[1]?.includes('*') };
              });

              window.WTG.drawCurrentPath(remapQueue);
            }

            return cached.apply(this, arguments);
          };
        })();
        window.walkTo = (function () {
          const cached = window.walkTo;

          return function () {
            const [x, y] = arguments;
            window.WTG.resetPathAfterChange(x, y);

            return cached.apply(this, arguments);
          };
        })();

        this.initLocalStorage();
        this.initObserver();
      } catch (e) {
        Logger.log(this.namespace, 'initError', 'Error during initialization...');
        console.error(e);
        return;
      }

      Logger.log(this.namespace, 'init', `Initialized.`);
      this.initSetup.wasInitialized = true;
    };
    initObserver = () => {
      const observerCallback = (mutations) => {
        const wasMapAdded = [...mutations].some(({ addedNodes }) =>
          [...addedNodes].some((node) => node?.id === 'map_container'),
        );

        if (wasMapAdded) this.drawTiles();
      };

      this.observer.instance = new MutationObserver(observerCallback);
      this.observer.instance.observe(this.observer.observedElement[0], this.observer.settings);
    };
    initLocalStorage = () => {
      const currentState = getJSON();
      this.colors = {
        tile: currentState[this.namespace].tilesColor.value,
        tileBorder: currentState[this.namespace].tilesBorderColor.value,
        tileAmbush: currentState[this.namespace].tilesAmbushColor.value,
        tilePath: currentState[this.namespace].tilesPathColor.value,
      };
    };
    createCanvas = () => {
      const canvas = document.createElement('canvas');
      canvas.id = 'walkable_tiles_container';
      $(canvas).css({
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
      });

      return canvas;
    };
    resetPathAfterChange = (x, y) => {
      const newPoint = `${x}-${y}`;
      const size = this.tileSize;

      if (
        this.currentlyWalkingTo &&
        this.currentlyWalkingTo !== newPoint &&
        this.walkable.list.length &&
        this.currentCanvas
      ) {
        const ctx = this.currentCanvas.getContext('2d');
        this.walkable.list.forEach((tile) =>
          this.drawTile({ ctx, tile, size, color: this.colors.tile, clear: true, forceClear: true }),
        );
      }

      this.currentlyWalkingTo = newPoint;
    };
    drawCurrentPath = async (walkableList) => {
      this.walkable.list = walkableList;
      this.walkable.lastTile = {
        x: character.longitude,
        y: character.latitude,
      };

      if (!this.currentCanvas) await this.drawTiles();

      const tileSize = this.tileSize;
      const ctx = this.currentCanvas.getContext('2d');
      const color = this.colors.tilePath;

      walkableList.forEach((tile) => {
        this.drawTile({ ctx, tile, size: tileSize, color });
      });
      if (this.walkable.lastTile) {
        this.drawTile({ ctx, tile: this.walkable.lastTile, size: tileSize, color: this.colors.tile });
      }
    };
    drawTile = ({ ctx, tile, size, color, clear = true, forceClear = false } = {}) => {
      const { x, y, isAmbush } = tile;
      const posX = x * size;
      const posY = y * size;

      if (clear) ctx.clearRect(posX, posY, size, size);

      ctx.fillStyle = this.colors.tileBorder;
      ctx.fillRect(posX, posY, size, size);
      ctx.fillStyle = !forceClear && isAmbush ? this.colors.tileAmbush : color;
      ctx.clearRect(posX + 1, posY + 1, size - 2, size - 2);
      ctx.fillRect(posX + 1, posY + 1, size - 2, size - 2);
    };
    drawTiles = () => {
      const canvas = this.createCanvas();
      const ctx = canvas.getContext('2d');
      const size = this.tileSize;

      $('#map_container').prepend(canvas);

      //auto resize to fill
      canvas.width = canvas.offsetWidth;
      canvas.height = canvas.offsetHeight;

      walkable_tiles.forEach((stringTile) => {
        const coordinates = stringTile.split('-').map((num) => parseInt(num));
        const tile = { x: coordinates[0], y: coordinates[1], isAmbush: false };

        this.drawTile({ ctx, tile, size, color: this.colors.tile, clear: false });
      });
      this.currentCanvas = canvas;
    };
    getContext = () => this.currentCanvas?.getContext('2d');
    getTileSize = () => this.tileSize;
  }

  window.WTG = new WTG();
  window.WTG.init();
})();
