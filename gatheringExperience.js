(() => {
  class GE {
    constructor(manager) {
      this.manager = manager;
      this.namespace = this.constructor.name;
      this.initSetup = {
        wasInitialized: false,
      };
      this.observer = {
        observedElement: $('#frame_container'),
        instance: null,
        settings: {
          childList: true,
          subtree: true,
        },
      };
      this.exp = {
        percentage: null,
        curr: null,
        max: null,
        relativeMax: null,
      };
      this.containerElement = $(`<div id="${this.namespace}-progressTracker" class="divblock_dark"></div>`);
    }

    init = () => {
      if (this.initSetup.wasInitialized) return;
      Logger.log(this.namespace, 'init', 'Initializing...');
      try {
        this.initObserver();
        this.injectCSS();
      } catch (e) {
        Logger.log(this.namespace, 'initError', 'Error during initialization...');
        console.error(e);
        return;
      }

      Logger.log(this.namespace, 'init', 'Initialized.');
      this.initSetup.wasInitialized = true;
    };
    injectCSS = () => {
      $('head').append(`
        <style>
          .${this.namespace}-progressBarContainer {
            position: absolute;
            width: 100%;
            height: 7px;
            bottom: -3px;
            left: 0;
            background: linear-gradient(180deg, hsl(41deg 75% 30%), hsl(38deg 70% 10%));
            border: 1px solid black;
          }
      
          .${this.namespace}-progressBar {
            position: inherit;
            width: 100%;
            height: 100%;
            left: 0;
            bottom: -1px;
            box-sizing: content-box;
            background: linear-gradient(180deg, hsl(41deg 75% 50%), hsl(38deg 70% 24%));
            border: inherit;
          }
        </style>
      `);
    };
    initObserver = () => {
      if (this.initSetup.wasInitialized) return;

      const observerCallback = (mutations) => {
        const hasTurnsLeft = mutations.filter((m) => m.target?.id?.toLowerCase() === 'turns_left').length;
        const hasOpened = mutations.some(
          (m) => m.target.parentNode.id?.toLowerCase() === 'frame_gathering' && $(m.target).find('.experience_bar')[0],
        );
        const hasClosed = mutations.some((m) =>
          [...m.removedNodes].some((node) => node.id?.toLowerCase() === 'frame_gathering'),
        );

        if (hasTurnsLeft || hasOpened) {
          const expBar = $('#frame_Gathering .experience_bar')[0];
          const { current_exp, next_exp } = expBar.attributes;

          setTimeout(() => {
            this.updateExperience(parseInt(current_exp.value), parseInt(next_exp.value));
          });
        }

        if (hasClosed) {
          this.exp = {
            percentage: null,
            curr: null,
            max: null,
            relativeMax: null,
          };
        }
      };

      this.observer.instance?.disconnect();
      this.observer.instance = new MutationObserver(observerCallback);
      this.observer.instance.observe(this.observer.observedElement[0], this.observer.settings);
    };

    getInterceptors = () => this.manager.ajaxInterceptors;
    calculateCurrentExp = (curr, currPercentage, max) => {
      if (!this.exp.max) this.exp.max = max;
      if (this.exp.max && this.exp.max < max) {
        this.exp = {
          ...this.exp,
          max,
          curr,
          relativeMax: null,
          percentage: 0,
        };
        currPercentage = 0;
      }

      const remaining = this.exp.max - curr;
      const relativeMax = (remaining * 100) / (100 - currPercentage);

      if (!this.exp.relativeMax || (this.exp.relativeMax && this.exp.relativeMax < relativeMax)) {
        this.exp.relativeMax = relativeMax;
      }

      this.exp.percentage =
        this.exp.curr !== null
          ? currPercentage + ((curr - this.exp.curr) / this.exp.relativeMax) * 100
          : currPercentage;

      const result = {
        current: ((this.exp.percentage * this.exp.relativeMax) / 100).toFixed(0),
        max: this.exp.relativeMax?.toFixed(0),
        currentPercentage: this.exp.percentage.toFixed(2),
        gained: this.exp.curr ? curr - this.exp.curr : 0,
      };

      this.exp.curr = curr;

      return result;
    };
    updateExperience = (currentExp = 0, nextExp = 0) => {
      if (this.exp.percentage === null) {
        const lastModal = this.getInterceptors().find(({ responseURL }) => responseURL.endsWith('modal/gathering'));
        const barWidth = $(lastModal.responseText).find('.experience_bar .bar_length')[0]?.style?.width;

        this.exp.percentage = parseFloat(barWidth);
      }

      // TODO: make [data.gained] an optional feature enabled by switch in options.
      const data = this.calculateCurrentExp(currentExp, this.exp.percentage, nextExp);
      const label = $(
        `<p>${data.current} / ${data.max} &nbsp; (${data.currentPercentage}%) ${
          data.gained ? `&nbsp;&nbsp;[+${data.gained}]` : ''
        }</p>`,
      );
      const progressBar = $(`
        <div class="${this.namespace}-progressBarContainer">
          <div class="${this.namespace}-progressBar" style="width: ${data.currentPercentage}%"></div>
        </div>
      `);

      const anchor = $('#frame_Gathering td.center');
      const isAppended = !!anchor.find(this.containerElement)[0];
      if (!isAppended) anchor.append(this.containerElement);

      this.containerElement.html(`${label[0].outerHTML}${progressBar[0].outerHTML}`);
    };
  }

  window.GE = new GE(window.AddonsManager);
  window.GE.init();
})();
