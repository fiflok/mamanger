(() => {
  class BTA {
    constructor() {
      this.namespace = this.constructor.name;
      this.initSetup = {
        wasInitialized: false,
      };
      this.sound = {
        blockPlayer: false,
        instance: null,
        notificationLimit: 0,
        interval: 500,
      };
      this.timeout = {
        instance: null,
      };
      this.observer = {
        observedElement: $('#frame_container'),
        instance: null,
        settings: {
          childList: true,
          subtree: true,
        },
      };
    }

    init = () => {
      if (this.initSetup.wasInitialized) return;

      Logger.log(this.namespace, 'init', 'Initializing...');

      try {
        this.initLocalStorage();
        this.initObserver();
      } catch (e) {
        Logger.log(this.namespace, 'initError', 'Error during initialization...');
        console.error(e);
        return;
      }

      Logger.log(this.namespace, 'init', 'Initialized.');
      this.initSetup.wasInitialized = true;
    };
    initLocalStorage = () => {
      if (this.initSetup.wasInitialized) return;

      const currentState = getJSON();
      const volume = currentState[this.namespace].volume.value;
      this.sound = {
        instance: new Audio(currentState[this.namespace].audioFile.value),
        notificationLimit: parseInt(currentState[this.namespace].notificationLimit.value),
        volume,
      };
      this.sound.instance.volume = Math.min(Math.max(volume, 0), 1);
    };
    initObserver = () => {
      const observerCallback = async (mutations) => {
        const mutationsList = [...mutations];
        const isTurnsLeft = mutationsList.filter((node) => node.target?.id?.toLowerCase() === 'turns_left')[0];
        const didPlayerLeft = mutationsList[0]?.removedNodes[0]?.id?.toLowerCase() === 'frame_gathering';

        if (isTurnsLeft) {
          this.sound.blockPlayer = false;
          const turnsLeft = parseInt(isTurnsLeft.target.textContent);
          if (turnsLeft <= this.sound.notificationLimit && !this.timeout.instance) {
            void await this.playSoundTimeout();
          }
        }
        if (didPlayerLeft) {
          this.sound.blockPlayer = true;
        }
      };
      this.observer.instance = new MutationObserver(observerCallback);
      this.observer.instance.observe(this.observer.observedElement[0], this.observer.settings);
    };
    playSoundTimeout = async () => {
      if (this.sound.blockPlayer) {
        clearTimeout(this.timeout.instance);
        this.timeout.instance = null;
        this.sound.instance.pause();
        this.sound.instance.currentTime = 0;
        return;
      }
      await this.sound.instance.play();
      this.timeout.instance = setTimeout(this.playSoundTimeout, this.sound.interval);
    };
  }

  window.BTA = new BTA();
  window.BTA.init();
})();
