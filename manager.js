// ==UserScript==
// @name         Addons Manager
// @namespace    http://tampermonkey.net/
// @version      1.1
// @author       fiflok
// @description  Okienko do zarządzania addonami.
// @license      MIT
// @match        https://www.norroth.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=norroth.com
// @grant        none
// @run-at       document-idle
// ==/UserScript==

(() => {
  const makeId = (length) => {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    return new Array(length)
      .fill(null)
      .map(() => chars.charAt(Math.floor(Math.random() * chars.length)))
      .join('');
  };

  const id = makeId(16);

  class AddonsManager {
    constructor(id) {
      this.initSetup = {
        counter: 0,
        limiter: 10,
        interval: 150,
        wasInitialized: false,
      };
      this.isOpen = false;
      this.forceClear = false;
      this.modal = null;
      this.namespace = this.constructor.name;
      this.observer = {
        instance: null,
        settings: {
          attributes: true,
          attributeFilter: ['style'],
        },
      };
      this.nodeTypes = ['text', 'checkbox', 'range-select'];
      this.ajaxInterceptors = [];
      this.addonsList = {
        setup: {
          version: "1.0.1",
        },
        WIH: {
          name: 'Who is here',
          link: 'https://static.staticsave.com/fiflok/jakaslista.js',
          isEnabled: {
            value: false,
          },
          modalPos: {
            x: 15,
            y: 180,
          },
        },
        BTA: {
          name: 'Broken tool alert',
          link: 'https://static.staticsave.com/fiflok/demonzik.js',
          isEnabled: {
            value: false,
          },
          audioFile: {
            isEditable: true,
            name: 'Ścieżka mp3',
            value: 'https://szkajpur.pl/files/sounds/eo.ogg',
            nodeType: 'text',
          },
          notificationLimit: {
            isEditable: true,
            name: 'Minimalna wytrzymałość (w turach)',
            value: 0,
            nodeType: 'text',
          },
          volume: {
            isEditable: false,
            name: 'Głośność powiadomienia',
            value: 1,
            nodeType: 'range-select',
          },
        },
        WTG: {
          name: 'Walkable tiles grid',
          link: 'https://static.staticsave.com/fiflok/kratki.js',
          isEnabled: {
            value: false,
          },
          tilesColor: {
            isEditable: true,
            name: 'Kolor kratek',
            value: 'rgba(255, 255, 255, 0.11)',
            nodeType: 'text',
          },
          tilesBorderColor: {
            isEditable: true,
            name: 'Kolor krawędzi kratek',
            value: 'rgba(0, 0, 0, 0.2)',
            nodeType: 'text',
          },
          tilesAmbushColor: {
            isEditable: true,
            name: 'Kolor kratek z zasadzką',
            value: 'rgba(255, 233, 0, 0.2)',
            nodeType: 'text',
          },
          tilesPathColor: {
            isEditable: true,
            name: 'Kolor kratek drogi postaci',
            value: 'rgba(255, 0, 0, 0.25)',
            nodeType: 'text',
          },
        },
        GP: {
          name: 'Gathering progress',
          link: '',
          isEnabled: {
            value: false,
          },
          displayLastGain: {
            isEditable: true,
            name: 'Pokazuj Ilość expa z ostatniej tury',
            value: false,
            nodeType: 'checkbox',
          },
        },
      };
    }

    init = async () => {
      if (this.initSetup.wasInitialized) return;
      if (!$ || !window.jQuery) {
        if (this.initSetup.counter === this.initSetup.limiter) {
          notification(`${this.namespace}: Brak jQuery na stronie, powiadom developera.`, 'red', true);
          return;
        }

        this.initSetup.counter++;
        return setTimeout(this.init, this.initSetup.interval);
      }
      if (!window.Logger) {
        await $.ajax({
          url: 'https://static.staticsave.com/fiflok/logger.js',
          type: 'GET',
          dataType: 'script',
          success: () => Logger.log(Logger.namespace, 'init', 'Initialized.'),
        });
      }
      Logger.log(this.namespace, 'init', 'Initializing...');

      const root = $(`#${this.namespace}`);
      if (root[0]) root.remove();

      this.injectCSS();
      this.initLocalStorage();
      this.initAjaxIntercepting();
      this.modal = this.createAddonWindow();
      this.initObserver();

      $('body').append(this.modal);
      Logger.log(this.namespace, 'init', 'Initialized.');
      await this.initEnabledScripts();
      this.initSetup.wasInitialized = true;
    };
    injectCSS = () => {
      if (this.initSetup.wasInitialized) return;

      $('head').append(`
          <style>
            #${this.namespace} {
              position: fixed;
              top: 160px;
              right: 0;
              transform: translateX(100%);
              transition: transform 0.3s ease;
              z-index: 1;
            }
            
            .${this.namespace}-trigger {
              position: absolute;
              width: 48px;
              height: 48px;
              background: black;
              left: -48px;
              border-radius: 10px 0 0 10px;
              display: flex;
              justify-content: center;
              align-items: center;
              cursor: url(https://www.norroth.com/styles/hand.cur) 4 0, pointer;
            }
            
            .${this.namespace}-trigger img {
              filter: invert(1);
              width: 36px;
              height: 36px;
            }
            
            .${this.namespace}-contentContainer {
              width: 450px;
              height: 600px;
              background: #44372f;
              border-bottom-right-radius: 10px;
              border: 24px outset transparent;
              border-image: url(https://www.norroth.com/images/layout/border-frame.png) 46 repeat;
              display: flex;
              flex-direction: column;
              align-items: center;
              gap: 8px;
              overflow: hidden;
            }
            
            .${this.namespace}-accordionsList {
              display: flex;
              flex-direction: column;
              width: 100%;
              overflow: hidden;
              overflow-y: auto;
            }
            
            .${this.namespace}-accordionItem {
              padding: 12px 0;
              border-bottom: 1px solid hsl(23deg 18% 15%);
              display: flex;
              flex-direction: column;
            }
            
            .${this.namespace}-accordionItem-header {
              display: flex;
              justify-content: space-between;
              padding: 8px 0;
            }
            
            .${this.namespace}-accordionItem-header img {
              width: 16px;
              height: 16px;
              filter: invert(1);
              transform: rotate(180deg);
              transition: transform 0.3s ease;
            }
            
            .${this.namespace}-accordionItem-content {
              display: flex;
              flex-direction: column;
              overflow: hidden;
              padding-top: 0;
              height: 0;
              gap: 8px;
            }
            
            .${this.namespace}-splitRow {
              display: flex;
              width: 100%;
              justify-content: space-between;
              align-items: center;
              gap: 8px;
            }
            
            .${this.namespace}-splitRow p {
              font-size: 12px;
              flex: 1;
            }
            
            .${this.namespace}-splitRow p + p {
              text-align: end;
            }
            
            .${this.namespace}-splitRow input[type='text'] {
              padding: 6px 10px;
              background: transparent;
              border: 1px solid hsl(23deg 18% 40%);
              border-radius: 8px;
              outline: 0;
            }
            .${this.namespace}-splitRow input[type='text']:disabled {
              background-color: hsl(0deg 0% 20%);
              color: hsl(0deg 0% 40%);
            }
            
            .switch {
              position: relative;
              display: inline-block;
              width: 50px;
              height: 24px;
            }
            .switch input {
              opacity: 0;
              width: 0;
              height: 0;
            }
            .slider {
              position: absolute;
              cursor: pointer;
              top: 0;
              left: 0;
              right: 0;
              bottom: 0;
              background-color: hsl(0, 100%, 60%, 1);;
              -webkit-transition: .4s;
              transition: .4s;
            }
            .slider:before {
              position: absolute;
              content: "";
              height: 18px;
              width: 17px;
              left: 4px;
              bottom: 3px;
              background-color: white;
              -webkit-transition: .4s;
              transition: .4s;
            }
            input:checked + .slider {
              background-color: hsl(120, 60%, 60%, 1);
            }
            input:focus + .slider {
              box-shadow: 0 0 1px #2196F3;
            }
            input:checked + .slider:before {
              -webkit-transform: translateX(26px);
              -ms-transform: translateX(26px);
              transform: translateX(26px);
              background-color: white;
            }
            .slider.round {
              border-radius: 34px;
            }
            .slider.round:before {
              border-radius: 50%;
            }
          </style>
        `);
    };
    initLocalStorage = () => {
      if (this.initSetup.wasInitialized) return;

      if (this.forceClear || !localStorage.getItem(this.namespace)) {
        Logger.log(this.namespace, "init", "Creating user config...");
        localStorage.setItem(this.namespace, JSON.stringify(this.addonsList));
        return;
      }

      const currentState = getJSON();
      if (currentState.setup?.version !== this.addonsList.setup.version) {
        this.updateUserConfig(currentState);
      }
    };
    initAjaxIntercepting = () => {
      if (this.initSetup.wasInitialized) return;

      const originalRequest = XMLHttpRequest.prototype.open;
      XMLHttpRequest.prototype.open = function () {
        this.addEventListener('load', function () {
          const { responseURL } = this;
          if (responseURL.endsWith('.mp3') || responseURL.endsWith('mp4')) return;

          window.AddonsManager.ajaxInterceptors.unshift(this);
          const arrayLength = window.AddonsManager.ajaxInterceptors.length;

          if (arrayLength > 100) {
            void window.AddonsManager.ajaxInterceptors.pop();
          }
        });
        originalRequest.apply(this, arguments);
      };
    };
    initObserver = () => {
      if (this.initSetup.wasInitialized) return;

      const observedElement = $('#transition_container');
      const observerCallback = (mutations) => {
        const top = parseFloat(mutations[0].target.style.top);
        const height = parseFloat(mutations[0].target.style.height);
        const mapElement = $('#map_container')[0];

        if (top === 0 && height === 100) {
          this.modal.css({
            zIndex: mapElement ? 1 : -1,
          });
        }
      };

      this.observer.instance = new MutationObserver(observerCallback);
      this.observer.instance.observe(observedElement[0], this.observer.settings);
    };
    initEnabledScripts = async () => {
      if (this.initSetup.wasInitialized) return;
      Logger.log(this.namespace, 'info', 'Downloading enabled scripts...');

      const currentState = getJSON();
      const enabledScripts = Object.entries(currentState)
        .filter(([name, config]) => name !== 'setup' && config.isEnabled.value)
        .map((config) => config[1].link);

      await Promise.all(enabledScripts.map(this.prepareAjaxPromise))
        .then(() => Logger.log(this.namespace, "info", "Downloaded all enabled scripts successfully!"))
        .catch((e) => {
          Logger.log(this.namespace, "error", "Error occured during scripts installation...");
          console.log(e);
        })
    };

    clearInterceptors = () => (this.ajaxInterceptors = []);
    prepareAjaxPromise = async (url) => {
      return await $.ajax({
        url: url,
        type: 'GET',
        crossDomain: true,
        dataType: 'script',
        error: () => {
          Logger.log(this.namespace, 'error', `Error during "${url}" download!`);
          notification(`${this.namespace}: Nie można było pobrać "${url}", powiadom developera.`, 'red', true);
        },
      });
    };
    setAddonOption = (id = null, key = null, value = null) => {
      if (id === null || key === null || value === null) {
        Logger.log(this.namespace, 'error', 'Error during saving addon options.');
        notification(`${this.namespace}: Wystąpił błąd podczas zapisu opcji, powiadom developera.`, 'red', true);
        return console.log(id, key, value);
      }

      const currentState = getJSON();

      const props = currentState[id][key];
      currentState[id][key] = { ...props, value };
      setJSON(currentState);
    };
    createAddonSwitch = (id) => {
      const currentState = getJSON();
      const isEnabled = currentState[id].isEnabled.value;

      const switchComponent = $(`
          <div class="${this.namespace}-splitRow">
            <p>Status dodatku: ${isEnabled ? 'włączony' : 'wyłączony'}</p>
            <label class="switch" data-addon="${id}">
              <input type="checkbox" ${isEnabled ? 'checked' : ''}>
              <span class="slider round"></span>
            </label>
          </div>
        `);
      switchComponent.on('click', (e) => {
        e.preventDefault();
        const { currentTarget } = e;
        const checkbox = $(currentTarget).find('input');
        const addon = $(currentTarget).find('label').data('addon');
        const isChecked = !checkbox.prop('checked');

        checkbox.prop('checked', isChecked);
        $(currentTarget.parentNode)
          .find("input[type='text']")
          .toArray()
          .forEach((node) => {
            node.disabled = !isChecked;
          });
        this.setAddonOption(addon, 'isEnabled', isChecked);
      });

      return switchComponent;
    };
    handleNodeType = (nodeType, data) => {
      switch(nodeType) {
        case 'checkbox':
          return this.createCheckbox(data);
        case 'range-select':
          return "TODO: rodzaj pola do zaimplementowania";
        case 'text':
        default:
          return this.createTextInput(data);
      }
    }
    createCheckbox = ({ id, key, name, value, isEnabled }) => {
      const component = $(`
        <div class="${this.namespace}-splitRow">
          <p>${name}</p>
          <p>TODO: Rodzaj pola do zaimplementowania</p>
        </div>
      `)

      return component;
    }
    createRangeSelect = ({ id, key, name, value, isEnabled }) => {
      const component = $(`
        <div class="${this.namespace}-splitRow">
          <p>${name}</p>
          <p>TODO: Rodzaj pola do zaimplementowania</p>
        </div>
      `)

      return component;
    }
    createTextInput = ({ id, key, name, value, isEnabled }) => {
      const component = $(`
          <div class="${this.namespace}-splitRow">
            <p>${name}</p>
            <input type="text" value="${value}" ${!isEnabled ? 'disabled' : ''} />
          </div>
        `);

      component.find('input').on('input', (e) => {
        const {
          target: { value },
        } = e;
        this.setAddonOption(id, key, value);
      });

      return component;
    };
    createAddonWindow = () => {
      if (this.initSetup.wasInitialized) return;

      const addonWindowTrigger = $(`
          <div class="${this.namespace}-trigger">
            <img src="https://www.svgrepo.com/show/470772/cog.svg" alt="" />
          </div>
        `).on('click', this.toggleAddonWindow);
      const addonMainWindow = $(`<div id="${this.namespace}"></div>`);
      const addonContentContainer = $(`
          <div class="${this.namespace}-contentContainer">
            <h1>Menedżer Dodatków</h1>
            <div class="${this.namespace}-accordionsList"></div>
          </div>
        `);

      const accordionItems = Object.entries(this.addonsList)
        .filter(([name]) => name !== "setup")
        .map((addon) => {
        const [id, { name, link }] = addon;
        const currentState = getJSON();
        const item = $(
          `<div class="${this.namespace}-accordionItem" data-is-open="false" data-download-Link="${link}"></div>`,
        );
        const header = $(`
          <div class="${this.namespace}-accordionItem-header">
            <h3>${name}</h3>
            <img src="https://www.svgrepo.com/show/472536/chevron-up.svg" alt="" />
          </div>
        `).on('click', (e) => {
          const isOpen = !item.data('isOpen');
          const { currentTarget } = e;

          item.data('isOpen', isOpen);
          $(currentTarget)
            .find('img')
            .css({
              transform: `rotate(${isOpen ? '0deg' : '180deg'})`,
            });
          item.find(`div.${this.namespace}-accordionItem-content`).animate({
            height: isOpen ? '100%' : '0',
          },500);
        });

        const content = $(`
            <div class="${this.namespace}-accordionItem-content"></div>
          `);
        const switchElement = this.createAddonSwitch(id);
        const inputsList = Object.entries(currentState[id])
          .filter((entry) => typeof entry[1] === 'object' && entry[1].isEditable)
          .map((entry) => {
            const { name, value, nodeType } = entry[1];
            const key = entry[0];
            const isEnabled = currentState[id].isEnabled.value;

            if (!this.nodeTypes.includes(nodeType)) {
              Logger.log(this.namespace, "error", "Unexpected node type, aborting...");
              throw new Error(JSON.stringify({ name, value, nodeType, isEnabled, addon: addon.name }, null, 4));
            }

            return this.handleNodeType(nodeType, { id, key, name, value, isEnabled });
          });

        content.append(switchElement);
        content.append(inputsList);
        item.append(header);
        item.append(content);
        return item;
      });

      addonContentContainer.find(`div.${this.namespace}-accordionsList`).append(accordionItems);
      addonMainWindow.append(addonWindowTrigger);
      addonMainWindow.append(addonContentContainer);
      return addonMainWindow;
    };
    toggleAddonWindow = () => {
      this.isOpen = !this.isOpen;
      $(`#${this.namespace}`).css({
        transform: `translateX(${this.isOpen ? '0%' : '100%'})`,
      });
    };

    configIteration = (currentCfg, newCfg) => {
      return Object.entries(newCfg).reduce((acc, curr) => {
        const [key, value] = curr;
        if (currentCfg[key] === undefined) {
          acc[key] = value;
          return acc;
        }

        if (typeof(currentCfg[key]) === "object" && typeof(value) === "object") {
          acc[key] = this.configIteration(currentCfg, value);
          return acc;
        }

        acc[key] = currentCfg[key];
        return acc;
      }, {})
    }
    updateUserConfig = (currentState) => {
      Logger.log(this.namespace, "info", "Updating user current config...")
      const updatedConfig = this.configIteration(currentState, this.addonsList);
      setJSON(updatedConfig);
      Logger.log(this.namespace, "info", "Updated.")
    }
  }

  const namespace = AddonsManager.name;

  window.setJSON = (json) => {
    if (!json) {
      Logger.log(namespace, 'error', 'Trying to write an empty JSON object.');
      notification(`${this.namespace}: Problem podczas zapisu do localStorage, powiadom developera.`, 'red', true);
      return;
    }
    localStorage.setItem(namespace, JSON.stringify(json));
  };
  window.getJSON = () => {
    return JSON.parse(localStorage.getItem(namespace)) || {};
  };

  console.clear();
  window.AddonsManager = new AddonsManager(id);
  void window.AddonsManager.init();
})();
